import { Component, OnInit } from '@angular/core';
import {TaskService} from '../Services/task.service';
import {UserService} from '../Services/user.service';
import {UserActionComponent} from './user-action/user-action.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  columnDefs = [
    { field: 'name' , editable: true },
    { headerName: 'first name', field: 'first_name', width: 500, editable: true },
    { field: 'email', editable: true},
    { headerName: 'Actions', field: 'action', cellRendererFramework: UserActionComponent }];
  rowData;

  constructor(private userService: UserService) {
    userService.getUsers().subscribe(tasks => {
      this.rowData = tasks.data;
    });
  }

  ngOnInit(): void {
  }

  create(): void {
    this.userService.create((<HTMLInputElement> document.getElementById('name')).value, (<HTMLInputElement> document.getElementById('first_name')).value, (<HTMLInputElement> document.getElementById('email')).value ).subscribe(
      resource => {
        this.userService.getUsers().subscribe(tasks => {
          this.rowData = tasks.data;
        });
      }
    );
  }

  onCellValueChanged(params: any): void {
    this.userService.save(params.data).subscribe(retour => {
    });
  }



}
