import { Component, OnInit } from '@angular/core';
import {UserService} from '../../Services/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-element',
  templateUrl: './user-element.component.html',
  styleUrls: ['./user-element.component.css']
})
export class UserElementComponent implements OnInit {

  user;

  constructor(private userService: UserService, private route: ActivatedRoute) {
    userService.getUser(parseInt(this.route.snapshot.paramMap.get('id'))).subscribe( param => {
      this.user = param.data;
    });
  }

  ngOnInit(): void {
  }

  remove(): void {
    this.userService.remove(this.user.id).subscribe(param => {
      document.location.href = '/user';
    });
  }

}
