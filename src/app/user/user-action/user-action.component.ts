import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ICellRendererAngularComp} from 'ag-grid-angular';

@Component({
  selector: 'app-user-action',
  templateUrl: './user-action.component.html',
  styleUrls: ['./user-action.component.css']
})
export class UserActionComponent  implements ICellRendererAngularComp {

  data: any;
  params: any;
  constructor(private http: HttpClient) {}

  agInit(params): void {
    this.params = params;
    this.data = params.value;
  }

  refresh(): boolean {
    return false;
  }

}
