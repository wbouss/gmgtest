import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TaskComponent} from './task/task.component';
import {UserComponent} from './user/user.component';
import {UserElementComponent} from './user/user-element/user-element.component';
import {TaskElementComponent} from './task/task-element/task-element.component';

const routes: Routes = [
  { path: '', component: TaskComponent },
  { path: 'user', component: UserComponent },
  { path: 'task/:id', component: TaskElementComponent },
  { path: 'user/:id', component: UserElementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
