import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public static urlBase = 'http://127.0.0.1:8000/';
  public static headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getUsers(): Observable<any>{
    return this.http.get(UserService.urlBase + 'api/user');
  }

  getUser(id: number): Observable<any>{
    return this.http.get(UserService.urlBase + 'api/user/' + id);
  }

  save(user: any): Observable<any>{
    return this.http.put(UserService.urlBase + 'api/user/' + user.id ,
      JSON.stringify({ name: user.name , first_name: user.first_name, email: user.email}),
      { headers: UserService.headers }
    );
  }

  create(n: string, fn: string, e: string): Observable<any>{
    return this.http.post( UserService.urlBase + 'api/user/',
      JSON.stringify({ name : n , first_name: fn, email: e}),
      { headers: UserService.headers }
    );
  }

  remove(id: number): Observable<any>{
    return this.http.delete(UserService.urlBase + 'api/user/' + id);
  }


}
