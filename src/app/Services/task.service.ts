import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  public static urlBase = 'http://127.0.0.1:8000/';
  public static headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getTask(id: number): Observable<any>{
    return this.http.get(TaskService.urlBase + 'api/task/' + id);
  }

  getTasks(): Observable<any>{
    return this.http.get(TaskService.urlBase + 'api/task');
  }

  save(task: any): Observable<any>{
    return this.http.put(TaskService.urlBase + 'api/task/' + task.id ,
      JSON.stringify({ name: task.name , description: task.description, status: task.status}),
      { headers: TaskService.headers }
       );
  }

  create(n: string, d: string, s: string): Observable<any>{
    return this.http.post( TaskService.urlBase + 'api/task/',
      JSON.stringify({ name : n , description: d, status: s}),
      { headers: TaskService.headers }
    );
  }

  remove(id: number): Observable<any>{
    return this.http.delete(TaskService.urlBase + 'api/task/' + id);
  }
}
