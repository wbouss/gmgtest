import { Component, OnInit } from '@angular/core';
import { TaskService } from '../Services/task.service';
import {UserActionComponent} from '../user/user-action/user-action.component';
import {TaskActionComponent} from './task-action/task-action.component';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  columnDefs = [
    { field: 'name' , editable: true },
    { field: 'description', width: 500, editable: true },
    { field: 'status', editable: true},
    { headerName: 'user', field: 'user_id', editable: true},
    { headerName: 'Actions', field: 'action', cellRendererFramework: TaskActionComponent }];

  rowData;

  constructor(private taskService: TaskService) {
    taskService.getTasks().subscribe(tasks => {
      this.rowData = tasks.data;
    });
  }

  ngOnInit(): void {
  }

  create(): void {
    this.taskService.create((<HTMLInputElement> document.getElementById('name')).value, (<HTMLInputElement> document.getElementById('description')).value, (<HTMLInputElement> document.getElementById('status')).value ).subscribe(
      resource => {
        this.taskService.getTasks().subscribe(tasks => {
          this.rowData = tasks.data;
        });
      }
    );
  }

  onCellValueChanged(params: any): void {
    this.taskService.save(params.data).subscribe(retour => {
    });
  }

}
