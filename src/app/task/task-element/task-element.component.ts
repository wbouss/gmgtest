import { Component, OnInit } from '@angular/core';
import {UserService} from '../../Services/user.service';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../../Services/task.service';

@Component({
  selector: 'app-task-element',
  templateUrl: './task-element.component.html',
  styleUrls: ['./task-element.component.css']
})
export class TaskElementComponent {

  task;

  constructor(private taskService: TaskService, private route: ActivatedRoute) {
    taskService.getTask(parseInt(this.route.snapshot.paramMap.get('id'))).subscribe( param => {
      this.task = param.data;
    });
  }

  remove(): void {
    this.taskService.remove(this.task.id).subscribe(param => {
      document.location.href = '/';
    });
  }

}
