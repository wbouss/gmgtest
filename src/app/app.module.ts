import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NavigationContentComponent } from './navigation/navigation-content/navigation-content.component';
import { TaskComponent } from './task/task.component';
import { AgGridModule } from 'ag-grid-angular';
import {HttpClientModule} from '@angular/common/http';
import { UserComponent } from './user/user.component';
import { UserElementComponent } from './user/user-element/user-element.component';
import { UserActionComponent } from './user/user-action/user-action.component';
import { TaskActionComponent } from './task/task-action/task-action.component';
import { TaskElementComponent } from './task/task-element/task-element.component';



@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NavigationContentComponent,
    TaskComponent,
    UserComponent,
    UserElementComponent,
    UserActionComponent,
    TaskActionComponent,
    TaskElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgGridModule.withComponents([UserActionComponent]),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
